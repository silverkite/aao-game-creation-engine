{
"error_perms_file_edit" : "Cannot edit trial file",
"error_perms_file_delete" : "Cannot delete trial file",

"error_perms_trial_read" : "Insufficient user privileges to read trial",
"error_perms_trial_manage_backups" : "Insufficient user privileges to manage trial backups",
"error_perms_trial_edit_secured_metadata" : "Insufficient user privileges to edit secured trial metadata",
"error_perms_trial_edit_metadata" : "Insufficient user privileges to edit trial metadata",
"error_perms_trial_create_someone_else" : "Insufficient user privileges to create trials for someone else",

"error_perms_sequence_add_trial" : "Insufficient user privileges to add this trial to a sequence",
"error_perms_sequence_add_sequence" : "Insufficient user privileges to add this sequence to a sequence",
"error_perms_sequence_edit" : "Insufficient user privileges to edit this sequence",
"error_perms_sequence_delete" : "Insufficient user privileges to delete this sequence",


"error_user_inactive": "Only an active logged-in user can perform this action",

"error_user_trial_language_unavailable" : "The language you selected is not available on AAO",
"error_user_trial_backup_no_slot" : "You do not have any backup slot available",

"error_user_sequence_trial_already_taken" : "Targeted trial is already part of another sequence",
"error_user_sequence_sequence_already_taken" : "Targeted sequence is already a child of another sequence",


"error_tech_v5_trial_language_edit_in_sequence" : "Language of a trial in a sequence cannot be changed",
"error_tech_v5_trial_author_edit_in_sequence" : "Author of a trial in a sequence cannot be changed",

"error_tech_v5_sequence_id_already_used" : "A sequence with this name already exists",
"error_tech_v5_sequence_empty" : "A sequence must contain at least one trial",
"error_tech_v5_sequence_incorrect_trial_author" : "A sequence can only contain trials from the same author",
"error_tech_v5_sequence_incorrect_trial_language" : "A sequence can only contain trials in the same language",
"error_tech_v5_sequence_nested" : "Nested sequences are not supported"
}
