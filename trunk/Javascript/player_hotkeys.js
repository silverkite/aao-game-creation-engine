/*
Ace Attorney Online (fork: silverkite, AAO: mercurialSK)
Player Hotkeys module

=== Currently only the following are mapped:
c - continue
s - switch between evidence and profiles

=== Only accepts single keystrokes for now

--
note to self: check dependencies

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'player_hotkeys',
	dependencies : ['player_actions'],
	init : function() {
		
		//function registerEventHandler(element, event, handler, capture)
		registerEventHandler(document.getElementById('title'), 'click', function()
		{
			alert('h');
		}, false);
		
		registerEventHandler(document, 'keydown', parseKeyDown, false);
		
	}
}));

//INDEPENDENT INSTRUCTIONS
function keyMap(key) {
	
	// Hotkey table
	// Entries correspond to key codes, following array contains IDs to be clicked
	var map = {
		// c
		67: ['proceed'],
		// s
		83: ['cr_evidence_switch', 'cr_profiles_switch'],
	};
	
	return map[key];
}

function parseKeyDown(e) {	
	var key = e.keyCode;
	
	// collecting array of mapped IDs
	var mappings = keyMap(key);
	
	if (mappings) {
		for (var i = 0; i < mappings.length; i++) {
			var id = mappings[i];
			var element = document.getElementById(id);
			
			// check if element is visible
			if (element.offsetWidth > 0 || element.offsetHeight > 0) {
				element.click();
				break;
			};
		};
	};
	
	return false;
}

//EXPORTED VARIABLES

//EXPORTED FUNCTIONS

//END OF MODULE
Modules.complete('player_hotkeys');