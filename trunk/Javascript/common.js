function defineGET()
{
	_GET = new Object();
	
	if(location.href.match(/\?/))
	{
		var elts = location.href.replace(/^([^?]*\?)/, '').split('&');
		
		for(var i = 0; i < elts.length; i++)
		{
			var subelts = elts[i].split('=');
			if(subelts.length > 1)
			{
				_GET[decodeURIComponent(subelts[0])] = decodeURIComponent(subelts[1]);
			}
			else
			{
				_GET[decodeURIComponent(subelts[0])] = null;
			}
		}
	}
}

var _GET;

defineGET();
